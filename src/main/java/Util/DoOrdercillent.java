/**
 * 统一下单接口调用
 *
 * @author Mr.Hou
 *
 */
package Util;
import org.apache.log4j.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;


public class DoOrdercillent {
	private static HttpServletResponse response;
	private static HttpServletRequest request;
	private static Logger logger = Logger.getLogger(DoOrdercillent.class);
	
	
	public static String getRequestURL(HashMap<String, String> map, String key) throws UnsupportedEncodingException {
        createSign(map,key);        
        StringBuffer sb = new StringBuffer();
        sb.append("<xml>"+'\n');
        String enc = TenpayUtil.getCharacterEncoding(request, response);
        Set es = map.entrySet();
        Iterator it = es.iterator();
        while(it.hasNext()) {
            Map.Entry entry = (Map.Entry)it.next();
            String k = (String)entry.getKey();
            String v = (String)entry.getValue();
            if ("attach".equalsIgnoreCase(k)||"body".equalsIgnoreCase(k)||"sign".equalsIgnoreCase(k)) {          
            	sb.append("<"+k+">"+v+"</"+k+">"+'\n');            
            }else {
                sb.append("<"+k+">"+v+"</"+k+">"+'\n');
            }
        }
        sb.append("</xml>");
        return sb.toString();
    }
	
	
	public static void createSign(HashMap<String, String> map,String key) {
		String sign="";
        StringBuffer stringSignTemp = new StringBuffer();
        Collection<String> keyset= map.keySet();
        List<String> list = new ArrayList<String>(keyset);    
        //对key键值按字典升序排序  
        Collections.sort(list);  
        for (int i = 0; i < list.size(); i++) {  
        	stringSignTemp.append(list.get(i)+"="+map.get(list.get(i))+"&");  
        }  
         stringSignTemp.append("key=" + key); //自己的API密钥            
         sign = MD5Util.MD5Encode(stringSignTemp.toString().replace(" ", ""),"utf-8").toUpperCase();
         map.put("sign", sign);  
         System.out.println(stringSignTemp);          
    }
	
	
	//统一下单
	public  synchronized static  String SendDate(
	   String appid,String mch_id,String device_info,String nonce_str, String body, String out_trade_no,
	   String total_fee,String spbill_create_ip,String notify_url,String trade_type	,String openid,String key
	   ) throws UnsupportedEncodingException, MalformedURLException{
		    	String prepay_id="";
		    	try{   
				    HashMap<String, String> map =new  HashMap<String, String>(); 
				    total_fee=deletpoint((Float.parseFloat(total_fee)*100)+"");
				    map.put("appid",appid);
				    map.put("mch_id", mch_id);               //商户号
				    map.put("device_info", device_info); //设备号
				    map.put("nonce_str", nonce_str);      //随机字符串
				    map.put("body", body);                        //商品描述
				    map.put("out_trade_no", out_trade_no);    //商家订单号
				    map.put("total_fee", total_fee);                    //商品金额,以分为单位
				    map.put("spbill_create_ip",spbill_create_ip);      //用户的公网ip  IpAddressUtil.getIpAddr(request)
				    map.put("notify_url", notify_url);
				    map.put("trade_type", trade_type);
				    map.put("openid", openid); 
				    String requestUrl = getRequestURL(map, key);//生成请求体并对参数签名
				    System.out.println("the send XML=================【"+requestUrl+"】");
				    logger.info("the send XML=================【"+requestUrl+"】");
				    URL url = new URL("https://api.mch.weixin.qq.com/pay/unifiedorder");
				    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
				    conn.setConnectTimeout(30000); // 设置连接主机超时（单位：毫秒)
				    conn.setReadTimeout(30000); // 设置从主机读取数据超时（单位：毫秒)
				    conn.setDoOutput(true); // post请求参数要放在http正文内，顾设置成true，默认是false
				    conn.setDoInput(true); // 设置是否从httpUrlConnection读入，默认情况下是true
				    conn.setUseCaches(false); // Post 请求不能使用缓存
				    // 设定传送的内容类型是可序列化的java对象(如果不设此项,在传送序列化对象时,当WEB服务默认的不是这种类型时可能抛java.io.EOFException)
				    conn.setRequestProperty("Content-Type","application/x-www-form-urlencoded");
				    conn.setRequestMethod("POST");// 设定请求的方法为"POST"，默认是GET
				    conn.setRequestProperty("Content-Length",requestUrl.length()+"");
				    String encode = "utf-8";
				    OutputStreamWriter out = new OutputStreamWriter(conn.getOutputStream(), encode);
				    out.write(requestUrl.toString());
				    out.flush();
				    out.close();
				    String result = PayCommonUtil.getOut(conn);
					XMLUtil xmlutil=new Util.XMLUtil();
				    //解析接口返回的xml
				    System.out.println("cilent return XML========================"+result);
				    logger.info("cilent return XML========================"+result);
				    prepay_id = xmlutil.OdXMLparseString(result); 
				    System.out.println(prepay_id.replace(" ", "").length());
				   
		    	}catch(Exception e){
			    	e.printStackTrace();
			        System.out.println("下单异常");
		    	}
				return prepay_id.replace(" ", "");
    }

    public  synchronized static  String programSendDate(
			String appid,String mch_id,String nonce_str,String sign ,String body, String out_trade_no,
			String total_fee,String spbill_create_ip,String notify_url,String trade_type,String key) throws UnsupportedEncodingException, MalformedURLException{
		String prepay_id="";
		try{
			HashMap<String, String> map =new  HashMap<String, String>();
			total_fee=deletpoint((Float.parseFloat(total_fee)*100)+"");
			map.put("appid",appid);
			map.put("mch_id", mch_id);               //商户号
			map.put("nonce_str", nonce_str);      //随机字符串
			map.put("body", body);                        //商品描述
			map.put("out_trade_no", out_trade_no);    //商家订单号
			map.put("total_fee", total_fee);                    //商品金额,以分为单位
			map.put("spbill_create_ip",spbill_create_ip);      //用户的公网ip  IpAddressUtil.getIpAddr(request)
			map.put("notify_url", notify_url);
			map.put("trade_type", trade_type);
			map.put("sign", sign);
			String requestUrl = getRequestURL(map, key);//生成请求体并对参数签名
			System.out.println("the send XML=================【"+requestUrl+"】");
			logger.info("the send XML=================【"+requestUrl+"】");
			URL url = new URL("https://api.mch.weixin.qq.com/pay/unifiedorder");
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setConnectTimeout(30000); // 设置连接主机超时（单位：毫秒)
			conn.setReadTimeout(30000); // 设置从主机读取数据超时（单位：毫秒)
			conn.setDoOutput(true); // post请求参数要放在http正文内，顾设置成true，默认是false
			conn.setDoInput(true); // 设置是否从httpUrlConnection读入，默认情况下是true
			conn.setUseCaches(false); // Post 请求不能使用缓存
			// 设定传送的内容类型是可序列化的java对象(如果不设此项,在传送序列化对象时,当WEB服务默认的不是这种类型时可能抛java.io.EOFException)
			conn.setRequestProperty("Content-Type","application/x-www-form-urlencoded");
			conn.setRequestMethod("POST");// 设定请求的方法为"POST"，默认是GET
			conn.setRequestProperty("Content-Length",requestUrl.length()+"");
			String encode = "utf-8";
			OutputStreamWriter out = new OutputStreamWriter(conn.getOutputStream(), encode);
			out.write(requestUrl.toString());
			out.flush();
			out.close();
			String result = PayCommonUtil.getOut(conn);
			XMLUtil xmlutil=new Util.XMLUtil();
			//解析接口返回的xml
			System.out.println("cilent return XML========================"+result);
			logger.info("cilent return XML========================"+result);
			prepay_id = xmlutil.OdXMLparseString(result);
			System.out.println(prepay_id.replace(" ", "").length());

		}catch(Exception e){
			e.printStackTrace();
			System.out.println("下单异常");
		}
		return prepay_id.replace(" ", "");
	}

	
	
	public static String deletpoint(String str){
		 int n = str.indexOf(".");
		  String  money ="";
		   if(n==-1){
			 money = str;
			}else{
				money=str.substring(0,str.indexOf("."));
			}
			System.out.println(money);
			return money;
	}
}

