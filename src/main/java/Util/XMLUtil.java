package Util;

import org.apache.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.Namespace;
import org.jdom.input.SAXBuilder;
import org.xml.sax.InputSource;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * xml工具类
 * @author miklchen
 *
 */
public class XMLUtil {
	private static Logger logger = Logger.getLogger(DoOrdercillent.class);
	/**
	 * 解析xml,返回第一级元素键值对。如果第一级元素有子节点，则此节点的值是子节点的xml数据。
	 * @param strxml
	 * @return
	 * @throws JDOMException
	 * @throws IOException
	 */
	public static Map doXMLParse(String strxml) throws JDOMException, IOException {
		if(null == strxml || "".equals(strxml)) {
			return null;
		}
		
		Map m = new HashMap();
		InputStream in = HttpClientUtil.String2Inputstream(strxml);
		SAXBuilder builder = new SAXBuilder();
		Document doc = builder.build(in);
		Element root = doc.getRootElement();
		List list = root.getChildren();
		Iterator it = list.iterator();
		while(it.hasNext()) {
			Element e = (Element) it.next();
			String k = e.getName();
			String v = "";
			List children = e.getChildren();
			if(children.isEmpty()) {
				v = e.getTextNormalize();
			} else {
				v = XMLUtil.getChildrenText(children);
			}
			
			m.put(k, v);
		}
		
		//关闭流
		in.close();
		
		return m;
	}
	
	/**
	 * 获取子结点的xml
	 * @param children
	 * @return String
	 */
	public static String getChildrenText(List children) {
		StringBuffer sb = new StringBuffer();
		if(!children.isEmpty()) {
			Iterator it = children.iterator();
			while(it.hasNext()) {
				Element e = (Element) it.next();
				String name = e.getName();
				String value = e.getTextNormalize();
				List list = e.getChildren();
				sb.append("<" + name + ">");
				if(!list.isEmpty()) {
					sb.append(XMLUtil.getChildrenText(list));
				}
				sb.append(value);
				sb.append("</" + name + ">");
			}
		}
		
		return sb.toString();
	}
	
	/**
	 * 获取xml编码字符集
	 * @param strxml
	 * @return
	 * @throws IOException 
	 * @throws JDOMException 
	 */
	public static String getXMLEncoding(String strxml) throws JDOMException, IOException {
		InputStream in = HttpClientUtil.String2Inputstream(strxml);
		SAXBuilder builder = new SAXBuilder();
		Document doc = builder.build(in);
		in.close();
		return (String)doc.getProperty("encoding");
	}
	
	public static String OdXMLparseString(String strxml ){
		String prepay_id="";       
			//解析淘宝返回的XML字符串
			 StringReader read = new StringReader(strxml);
		        //创建新的输入源SAX 解析器将使用 InputSource 对象来确定如何读取 XML 输入
		     InputSource source = new InputSource(read);
		        //创建一个新的SAXBuilder
		      SAXBuilder sb = new SAXBuilder();
		        try {
		            //通过输入源构造一个Document
		            Document doc = sb.build(source);
		            //取的根元素
		            Element root = doc.getRootElement();
		            // System.out.println(root.getName());//输出根元素的名称（测试）
		            //得到根元素所有子元素的集合
		            List jiedian = root.getChildren();
		            //获得XML中的命名空间（XML中未定义可不写）
		            Namespace ns = root.getNamespace();
		            String return_msg = root.getChild("return_msg",ns).getText();
		            String return_code = root.getChild("return_code",ns).getText();
		            System.out.println("return_code========"+return_code);
		            logger.info("return_code========"+return_code);
		            System.out.println("return_msg========"+return_msg);
		            logger.info("return_msg========"+return_msg);
		            if(return_code.equals("SUCCESS")&&return_msg.equals("OK")){
		            	 String result_code = root.getChild("result_code",ns).getText();	
		                 System.out.println("result_code========"+result_code);
		                 logger.info("result_code========"+result_code);
		                 if(result_code.equals("SUCCESS")){
			                	 prepay_id = root.getChild("prepay_id",ns).getText();
			 		             System.out.println("prepay_id========"+prepay_id.replace(" ", ""));
			 		            logger.info("prepay_id========"+prepay_id.replace(" ", ""));
		                 }
		            }else{
		            	System.out.println("调用接口错误！！！！！！！！！！");
		            }
	}catch(Exception e){
		
	  }
				return prepay_id;
	}
}
