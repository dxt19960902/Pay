package Util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;

public class PayCommonUtil {
	public static String getOut(HttpURLConnection conn) throws IOException{

        if (conn.getResponseCode() != HttpURLConnection.HTTP_OK) {

            return null;
            }
        
            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));

            String line = "";

            StringBuffer strBuf = new StringBuffer();

            while ((line = in.readLine()) != null) {

                strBuf.append(line).append("\n");

            }

            in.close();

            return  strBuf.toString().trim();

    
	
        
      
}
}