package service;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
@Service
public class AuthorizeImpl implements Authorizel {
    // appid
    public static final String APP_ID = "wx26f45ef55873061a";
    // 应用密钥AppSecret
    public static final String APP_SECRET = "3a91012538e93a131114cbd4bb8fceed";
   // public String notify_url;// 通知地址
    public HttpServletRequest request;
    public HttpServletResponse response;
    HttpSession session = null;
    String code;
    /**
     * 获取code
     * @throws UnsupportedEncodingException
     */
    public void obtain_code(String notify_url) throws UnsupportedEncodingException {
        session.setAttribute("notify_url",notify_url);
        String redirect_uri = "http://test.bndjcr.com/weChat/obtain_openid?notify_url="
                + notify_url;
        redirect_uri = URLEncoder.encode(redirect_uri, "utf-8");
        String url = "https://open.weixin.qq.com/connect/oauth2/authorize?"
                + "appid=" + APP_ID + "&response_type=code"
                + "&scope=snsapi_userinfo" + "&state=123" + "&redirect_uri="
                + redirect_uri + "#wechat_redirect";
        System.out.println(url);
        try {
            response.sendRedirect(url);

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("获取异常");
        }
    }
    /*
     * ==========================================================================
     * =============== 通过url获取用户openid
     */
    public void obtain_openid() {
        String notify_url= (String) session.getAttribute("notify_url");
        System.out.println("code==========" + code);
        String url = "https://api.weixin.qq.com/sns/oauth2/access_token?"
                + "appid=" + APP_ID + "&secret=" + APP_SECRET + "&code=" + code
                + "&grant_type=authorization_code";
        String openid = "";
        String token = "";
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            HttpClient client = new HttpClient();
            System.out.println(url);
            PostMethod post = new PostMethod(url);
            post.getParams().setContentCharset("utf-8");
            // 发送http请求
            String respStr = "";
            client.executeMethod(post);
            respStr = post.getResponseBodyAsString();
            System.out.println("respStr:" + respStr);
            Map mapResult = objectMapper.readValue(respStr, Map.class);
            if (mapResult.get("errcode") != null) {
                System.out.println("mapResult.get(errcode)===" + mapResult.get("errcode"));
                System.out.println("获取openid失败");
            } else {
                System.out.println("openid===========" + mapResult.get("openid"));
                openid = (String) mapResult.get("openid");
                token = (String) mapResult.get("access_token");
                try {
                    String temurl = notify_url + "?openid=" + openid + "&token=" + token;
                    System.out.println("temurl:" + temurl);
                    response.sendRedirect(temurl);
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("响应异常");
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("获取异常");
        }
    }
    /**
     * 获取用户信息
     * */
    public void queryUserInfo(String OPENID,String ACCESS_TOKEN) {
        String url = "https://api.weixin.qq.com/sns/userinfo" + "?access_token=" + ACCESS_TOKEN + ""
                + "&openid=" + OPENID + "" + "&lang=zh_CN";
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            HttpClient client = new HttpClient();
            System.out.println(url);
            PostMethod post = new PostMethod(url);
            post.getParams().setContentCharset("utf-8");
            // 发送http请求
            String respStr = "";
            client.executeMethod(post);
            respStr = post.getResponseBodyAsString();
            System.out.println("respStr:" + respStr);
            Map mapResult = objectMapper.readValue(respStr, Map.class);
            if (mapResult.get("errcode") != null) {
                System.out.println("mapResult.get(errcode)===" + mapResult.get("errcode"));
                System.out.println("获取openid失败");
            } else {
                PrintWriter pw = null;
                try {
                    RequestAttributes ra = RequestContextHolder.getRequestAttributes();
                    ServletRequestAttributes sra = (ServletRequestAttributes) ra;
                    pw = sra.getResponse().getWriter();
                    pw.print(respStr);
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    pw.flush();
                    pw.close();
                }
            }
        }  catch (IOException e) {
            e.printStackTrace();
            System.out.println("获取异常");
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("获取其他异常");
        }
    }

}
