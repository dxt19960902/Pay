package service;

public interface WechatProgramPayService {
   //预支付
    String getPrepay_id(String total_fee);
    String obtain_notifywx();
    String getSign(String prepay_id);
    String getNonceStr();
}
