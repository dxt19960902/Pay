package service;

import java.io.UnsupportedEncodingException;

public interface Authorizel {
    void obtain_code(String notify_url)  throws UnsupportedEncodingException;
    void obtain_openid();
    void queryUserInfo(String openid,String access_token);
}
