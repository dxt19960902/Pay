package service;

public interface WechatPayService {
    //获得预支付订单id
    String getPrepay_id (String openid,String total_fee);
    //请求支付
    void obtain_obParameter(String prepayid);
    //获取异步通知
    String obtain_notifywx();

}
