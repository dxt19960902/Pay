package service;

import Util.*;
import com.google.gson.GsonBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.MalformedURLException;
import java.util.*;
@Service
public class WechatProgramPayServiceImpl  implements WechatProgramPayService{
    /**
     *
     * 微信公众号支付参数
     */
    // appid
    public static final String APP_ID = "wx26f45ef55873061a";
    // 商户号
    public static final String MCH_ID = "1314811001";
    // API密钥
    public static final String KEY = "3a91012538e93a131114cbd4bb8fceed";//
    // 支付完成后的回调处理页面
    public static final String NOTIFY_URL = "http://test.bndjcr.com/ProgramWeChatPay/obtain_notifywx";
    public HttpServletRequest request;
    public HttpServletResponse response;
    public String nonce_str;// 随机字符串
    public String sign;// 签名
    public String body;// 商品描述
    public String out_trade_no;// 商户订单号
    public String spbill_create_ip;// 终端IP
    public String time_start;// 交易起始时间
    public String trade_type;// 交易类型
    private String resContent = "";
    // 获取微信需要的参数package--prepay_id参数值
    public String getPrepay_id(String total_fee) {

        // -------------生成预支付单 begin-----------------------
        RequestAttributes ra = RequestContextHolder.getRequestAttributes();
        ServletRequestAttributes sra = (ServletRequestAttributes) ra;
        request = sra.getRequest();
        nonce_str = WechatPayUyil.getRandomStringByLength(32);
        out_trade_no = WechatPayUyil.getOutTradeNo();
        spbill_create_ip = request.getRemoteAddr();
        time_start = TenpayUtil.getCurrTime();
        trade_type = "JSAPI";// 交易类型
        System.out
                .println("==================================================================="
                        + total_fee
                        + "===================================================================");
        body = "自定义";
        String device_info = "WEB";
        String prepay_id = null;
        System.out.println("APP_ID:" + APP_ID);
        System.out.println("MCH_ID:" + MCH_ID);
        System.out.println("device_info:" + device_info);
        System.out.println("nonce_str:" + nonce_str);
        System.out.println("body:" + body);
        System.out.println("out_trade_no:" + out_trade_no);
        System.out.println("total_fee:" + total_fee);
        System.out.println("spbill_create_ip:" + spbill_create_ip);
        System.out.println("NOTIFY_URL:" + NOTIFY_URL);
        System.out.println("trade_type:" + trade_type);
        System.out.println("KEY:" + KEY);
        try {
            String noncestr = WechatPayUyil.getRandomStringByLength(32);
            SortedMap<String, String> prePayParams = new TreeMap<String, String>();
            prePayParams.put("appId", APP_ID);
            prePayParams.put("nonceStr", noncestr);
            prePayParams.put("mch_id", MCH_ID);
            prePayParams.put("body", body);
            prePayParams.put("device_info", device_info);
            // 生成签名
            sign = WechatPayUyil.getSign(prePayParams, KEY);
            prePayParams.put("paySign", sign);
            System.out.println("sign=====================" + sign);
            prepay_id = DoOrdercillent.programSendDate(APP_ID, MCH_ID, nonce_str,
                    sign, body, out_trade_no, total_fee, spbill_create_ip,
                    NOTIFY_URL, trade_type,  KEY);
        } catch (Exception e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        SortedMap<String, String> prePayParams = new TreeMap<String, String>();

        PrintWriter pw = null;
        try {
            sra.getResponse().setCharacterEncoding("UTF-8");
            pw = sra.getResponse().getWriter();
            String json = new GsonBuilder().disableHtmlEscaping().create()
                    .toJson(prePayParams);
            pw.print(json);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            pw.flush();
            pw.close();
        }
        return  prepay_id;
    }
    /**
     * 获取异步通知
     */
    public String obtain_notifywx() {
        //logger.info("进入notifywx");
        RequestAttributes ra = RequestContextHolder.getRequestAttributes();
        ServletRequestAttributes sra = (ServletRequestAttributes) ra;
        request = sra.getRequest();
        response = sra.getResponse();

        // ---------------------------------------------------------
        // 微信支付通知（后台通知）示例，商户按照此文档进行开发即可
        // ---------------------------------------------------------

        // 创建支付应答对象

        ResponseHandler resHandler = new ResponseHandler(request, response);
        resHandler.setKey(KEY);

        BufferedReader reader;
        try {
            reader = new BufferedReader(new InputStreamReader(
                    request.getInputStream(), "UTF-8"));
            // 获取应答内容
            resContent = HttpClientUtil.bufferedReader2String(reader);
            // 关闭流
            reader.close();
        } catch (UnsupportedEncodingException e1) {
            e1.printStackTrace();
        } catch (IOException e1) {
            e1.printStackTrace();
        }

        Map<String, String> map = OrderInfoUtilWX.readStringXmlOut(resContent);
        String result_code = map.get("return_code");
        // 判断签名及结果
        if ("SUCCESS".equals(result_code)) {
            String ids = "";
            // ------------------------------
            // 即时到账处理业务开始
            // ------------------------------
            // 处理数据库逻辑
            // 注意交易单不要重复处理
            // 注意判断返回金额

            // ------------------------------
            // 即时到账处理业务完毕
            // ------------------------------


            // logger.info("即时到账支付成功");
            SortedMap<String, String> params = new TreeMap<String, String>();
            params.put("return_code", "SUCCESS");
            String notifyResult = returnResultToWX(params);
            // 给财付通系统发送成功信息，财付通系统收到此结果后不再进行后续通知
            try {
                response = sra.getResponse();
                response.setHeader("Content-Type",
                        "application/json;charset=UTF-8");
                response.getWriter().print(notifyResult);
                response.getWriter().flush();
                if (!map.get("out_trade_no").equals("")) {
                    /*
                     * 到账成功业务逻辑开始
                     *
                     * */

                }

                else {
                    // logger.info("即时到账支付失败");
                }
            } catch (Exception e1) {
                e1.printStackTrace();
            }
        }
        return null;

    }

    // 异步接收支付结果返回应答结果
    public String returnResultToWX(SortedMap params) {
        // 设置返回参数
        String postData = "<xml>";
        Set es = params.entrySet();
        Iterator it = es.iterator();
        while (it.hasNext()) {
            Map.Entry entry = (Map.Entry) it.next();
            String k = (String) entry.getKey();
            String v = (String) entry.getValue();
            if (k != "appkey") {
                if (postData.length() > 1)
                    postData += "<" + k + ">" + v + "</" + k + ">";
            }
        }
        postData += "<sign>" + sign + "</sign>";

        postData += "</xml>";
        System.out.println("return data=" + postData);
        return postData;
    }
    public String getSign(String prepay_id){
        SortedMap<String, String> prePayParams = new TreeMap<String, String>();
        String timestamp = TenpayUtil.getUnixTime(new Date()) + "";
        // 签名参数列表
        // 网页
        String noncestr = WechatPayUyil.getRandomStringByLength(32);
        prePayParams.put("appId", APP_ID);
        prePayParams.put("nonceStr", noncestr);
        prePayParams.put("timeStamp", timestamp);
        prePayParams.put("signType", "MD5");
        prePayParams.put("package", "prepay_id=" + prepay_id);
        // 生成签名
        sign = WechatPayUyil.getSign(prePayParams, KEY);
        prePayParams.put("paySign", sign);
        System.out.println("sign=====================" + sign);
        return sign;
    }
    //随机字符串
    public String getNonceStr(){
        String noncestr = WechatPayUyil.getRandomStringByLength(32);
        return noncestr;
    }
}
