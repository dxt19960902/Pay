package controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import service.WechatProgramPayService;

@RestController
@RequestMapping("/ProgramWeChatPay")
public class WechatProgramController {
    private final WechatProgramPayService service;

    public WechatProgramController(WechatProgramPayService service) {
        this.service = service;
    }

    //生成预支付订单
    @GetMapping("/getPrepay_id")
    public String getPrepay_id(String total_fee)  {
        return service.getPrepay_id(total_fee);
    }
    //请求支付
    @GetMapping("/getSign")
    public void getSign(String prepay_id)  {
        service.getSign(prepay_id);
    }
    //获取通知
    @GetMapping("/obtain_notifywx")
    public void obtain_notifywx()  {
        service.obtain_notifywx();
    }
    //获取随机字符串
    @GetMapping("/getNonceStr")
    public void getNonceStr()  {
        service.getNonceStr();
    }
}
