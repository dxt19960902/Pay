package controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import service.Authorizel;

import java.io.UnsupportedEncodingException;
@RestController
@RequestMapping("/weChat")
public class AuthorizelController {
    private final Authorizel authorizelService;

    public AuthorizelController(Authorizel authorizelService) {
        this.authorizelService = authorizelService;
    }

    @GetMapping("/obtain_code")
    public void obtain_code(String notify_url) throws UnsupportedEncodingException{
            authorizelService.obtain_code(notify_url);
    }
    @GetMapping("/obtain_openid")
    public void obtain_openid(){
        authorizelService.obtain_openid();
    }
    @GetMapping("/queryUserInfo")
    public void queryUserInfo(String openid,String access_token){
        authorizelService.queryUserInfo(openid,access_token);
    }
}
