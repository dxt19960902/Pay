package controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import service.WechatPayService;

import java.io.UnsupportedEncodingException;
@RestController
@RequestMapping("/weChatPay")
public class WechatPayController {
private final WechatPayService payService;

    public WechatPayController(WechatPayService payService) {
        this.payService = payService;
    }
    //生成预支付订单
    @GetMapping("/getPrepay_id")
    public String getPrepay_id(String openid,String total_fee)  {
        return payService.getPrepay_id(openid,total_fee);
    }
    //请求支付
    @GetMapping("/obtain_obParameter")
    public void obtain_obParameter(String prepay_id)  {
         payService.obtain_obParameter(prepay_id);
    }
    //获取通知
    @GetMapping("/obtain_notifywx")
    public void obtain_notifywx()  {
        payService.obtain_notifywx();
    }
}
